import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { ReportsComponent } from './components/reports/reports.component';
import { ReportingRoutingModule } from './reporting-routing.module';
import { CertificatedCoursesReportComponent } from './components/certificated-courses-report/certificated-courses-report.component';
import { CoursesByCategoriesReportComponent } from './components/courses-by-categories-report/courses-by-categories-report.component';

@NgModule({
  declarations: [ReportsComponent, CertificatedCoursesReportComponent, CoursesByCategoriesReportComponent],
  imports: [
    CommonModule,
    NgxChartsModule,
    ReportingRoutingModule
  ]
})
export class ReportingModule {
}

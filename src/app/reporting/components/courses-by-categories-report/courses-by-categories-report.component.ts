import { Component, OnInit } from '@angular/core';
import { take } from 'rxjs/operators';
import { CategoriesService } from '../../../categories/services/categories.service';

@Component({
  selector: 'lq-courses-by-categories-report',
  templateUrl: './courses-by-categories-report.component.html',
  styleUrls: ['./courses-by-categories-report.component.scss']
})
export class CoursesByCategoriesReportComponent implements OnInit {

  data = [];

  colorScheme = {
    domain: ['#5AA454', '#A10A28', '#C7B42C', '#AAAAAA', '#112A32']
  };

  constructor(private categoriesService: CategoriesService) {
  }

  ngOnInit(): void {
    this.categoriesService.getAll$().pipe(
      take(1)
    ).subscribe((categories) => {
      const result = [];
      for (const category of categories) {
        result.push({
          name: category.name,
          value: category.courses.length
        })
      }

      this.data = result;
    })
  }

}
